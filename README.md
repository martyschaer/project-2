# Project 2
This repository contains all materials relating to the Project 2 for the 8th semester at BFH.

## Files
- `README.md` - useful information about the repository
- `ARTICLE.md` - contains the main project report
- `JOURNAL.md` - the work journal
- `PLAN.ods` - the project plan / timeline
- `LINKS.md, NOTES.md` collection of useful information for working on the project

## CI/CD
The Markdown files are compiled to PDFs using Pandoc at every commit.
[Download the latest PDFs](https://gitlab.com/martyschaer/project-2/builds/artifacts/master/download?job=PDF)
