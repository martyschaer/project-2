# Links
This document contains links to resources or materials that may be referenced during the project.

## Articles
- [Microsoft, Oracle and Salesforce join a push for digital vaccination credentials](https://www.nytimes.com/live/2021/01/14/business/us-economy-coronavirus#covid-vaccine-credentials-microsoft)
- [Criteria for development of vaccine passport](https://royalsociety.org/-/media/policy/projects/set-c/set-c-vaccine-passports.pdf)
- [Israel und der Impfpass](https://www.br.de/nachrichten/kultur/israel-und-der-impfpass-kultur-genuss-wie-frueher,SQORAO6)

## Websites
- [Schweizer MeineImpfungen Stiftung](https://www.meineimpfungen.ch/)
- [Anonymous Credentials](https://github.com/cliqz-oss/anonymous-credentials)
- [European Commision announced 'Digital Green Certificate' 2021-03-17](https://ec.europa.eu/info/live-work-travel-eu/coronavirus-response/safe-covid-19-vaccines-europeans/covid-19-digital-green-certificates_en)
- [Swiss ID Card](https://de.wikipedia.org/wiki/Identit%C3%A4tskarte_(Schweiz))
- [EU-DGC Guidelines](https://ec.europa.eu/health/ehealth/covid-19_en)

## Specs
- [ICAO Passport Specification](https://www.icao.int/publications/pages/publication.aspx?docnum=9303)
- [Spec for DGC](https://ec.europa.eu/health/sites/health/files/ehealth/docs/trust-framework_interoperability_certificates_en.pdf)
- [hl7 FHIR Immunization](https://www.hl7.org/fhir/immunization.html)
- [ch-vacd vaccine ValueSet](http://build.fhir.org/ig/ehealthsuisse/ch-vacd/ValueSet-ch-vacd-vaccines-vs.html)

## Swiss Implementation
- [Examples](https://github.com/admin-ch/CovidCertificate-Examples)
- [Api Doc](https://github.com/admin-ch/CovidCertificate-Apidoc)
- [Documents](https://github.com/admin-ch/CovidCertificate-Documents)
