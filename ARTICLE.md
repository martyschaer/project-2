---
title: 'Project 2'
subtitle: 'Vaccination Certificates'
author:
  - Severin Kaderli
  - Marius Schär
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lecturer:
  - Prof. Dr. Annett Laube
  - Prof. Dr. Reto Koenig
lang: "en-US"
toc: true
lot: true
lof: true
main-color: 4caf50
rule-color: 4caf50
link-color: 4caf50
bibliography: citations.bib
glossary: true
glossary-file: glossary.tex
links-as-notes: true
...

# Introduction
Due to the ongoing COVID-19 pandemic, the issue of a "Proof of Vaccination"
has been raised in multiple countries.
In this project, we conceptualized our own Vaccination Certificate with the
goal of implementing it during our bachelor thesis.
A Vaccination Certificate must not be forgable, it must protect the
Certificate Owner's privacy, and it must be easy to verify.

We examined and compared several other (proposed) solutions to this problem,
and looked at the European Union's Digital COVID Certificate
as well as Switzerland's CovidCertificate in detail.

We believe that our solution has several benefits over the EU's solution
with regards to privacy and availability,
as well as interoperability with existing health systems
due our use of the FHIR standard (Fast Healthcare Interoperability Resources).

We did not examine the political issues surrounding these certificates,
instead focusing on the technical aspects.

![An example of our Vaccination Certificate](assets/certificate.png){width=70%}

# Personas
## Issuer (Alice)
Alice issues Vaccination Certificates to vaccinated persons.
This can occur either at the time of vaccination or afterwards,
using the administrative system used for planning vaccination appointments
as a source of vaccination information.

## Trust Root (Bob)
Bob is a trusted central authority, such as the FOPH or HIN,
which signs Alice' key pair.
Thus Victor only needs to trust Bob in order to verify a Vaccination Certificate.

## Certificate Owner (Charlie)
Charlie is a vaccinated person carrying a Vaccination Certificate.
He would like to use the certificate in order to travel internationally
or attend high-risk^[such as indoor events with a large number of people.] events.

## Verifier (Victor)
Victor wants to verify Vaccination Certificates.
This might be done by someone at a border enforcing quarantine rules,
or at the entrance of an event.

## Attackers
There are different kinds of attackers with different motives:

### Integrity (Mallory)
Mallory wants to compromise the integrity of a Vaccination Certificate,
attempting to create a valid certificate of her own.

### Privacy (Mike)
Mike wants to compromise the privacy of a Vaccination Certificate in order to:

- learn health or movement data about Charlie
- learn all or a subset of people that have received a Vaccination Certificate

### Availability (Trudy)
Trudy wants to prevent:

- Alice from issuing a Vaccination Certificate to Charlie
- Victor from verifying Charlies Vaccination Certificate

# Requirements
## Functional Requirements
| ID  | Description                                                       |
|:----|:------------------------------------------------------------------|
| F01 | _Alice_ enters _Charlie_'s immunization data.                     |
| F02 | _Alice_ verifies _Charlie_'s immunization data is correct.        |
| F03 | _Alice_ issues a VC based on _Charlie_'s data.                    |
| F04 | _Alice_ gives the VC to _Charlie_.                                |
| F05 | _Charlie_ uses the VC to prove to _Victor_ that he is vaccinated. |
| F06 | _Alice_ renews _Charlie_'s VC.                                    |

: Functional Requirements

### F01 Data Entry
| F01         | Data Entry                                                                                   |
|:------------|:---------------------------------------------------------------------------------------------|
| Scenario    | _Alice_ enters _Charlie_'s immunization data.                                                |
| Description | _Alice_ enters _Charlie_'s immunization data into the system.                                |
|             | This data includes the following based on the FHIR Immunization resource[@fhirImmunization]: |
|             | - vaccination date                                                                           |
|             | - vaccine lot number                                                                         |
|             | - a patient identifier (_Charlie_'s passport document number)                                |
|             | - manufacturer of the vaccine                                                                |
|             | - dose number                                                                                |
|             | - recommended number of doses                                                                |
|             | - the disease targeted by the vaccine (in this case, always COVID-19)                        |
| Actors      | Alice, Charlie                                                                               |

: Functional Requirement #01

\newpage

### F02 Data Verification
| F02         | Data verification.                                         |
|:------------|:-----------------------------------------------------------|
| Scenario    | _Alice_ verifies _Charlie_'s immunization data is correct. |
| Description | _Alice_ checks if _Charlie_'s data is correct.             |
|             | This includes verifying the patient identifier             |
|             | matches a valid passport presented by _Charlie_.           |
| Actors      | Alice, Charlie                                             |

: Functional Requirement #02

### F03 Vaccination Certificate Issuance
| F03         | Vaccination Certificate issuance.                      |
|:------------|:-------------------------------------------------------|
| Scenario    | _Alice_ issues a VC to _Charlie_.                      |
| Description | _Alice_ issues a Vaccination Certificate to _Charlie_. |
| Actors      | Alice, Charlie                                         |

: Functional Requirement #03

### F04 Vaccination Certificate Handout
| F04         | Vaccination Certificate handout                                |
|:------------|:---------------------------------------------------------------|
| Scenario    | _Alice_ gives the complete VC to _Charlie_.                    |
| Description | _Charlie_ can take (and present) the VC in the following ways: |
|             | - Printed on paper                                             |
|             | - Digitally, as a PDF                                          |
|             | - Digitally, by scanning a QR-Code                              |
| Actors      | Alice, Charlie                                                 |

: Functional Requirement #04

\newpage

### F05 Proof of Vaccination
| F05         | Proof of vaccination                                                    |
|:------------|:------------------------------------------------------------------------|
| Scenario    | _Charlie_ proves to _Victor_ that he is vaccinated                      |
|             | in order to attend an event.                                            |
| Description | At the entrance of the event _Victor_ must ensure that only             |
|             | vaccinated people are able to enter. The verification works as follows: |
|             | 1) _Victor_ scans the VC provided by an attendee                        |
|             | 2) _Victor_ scans the passport[@doc9303] provided by the attendee       |
|             | 3) The verification app tells _Victor_ if the presented combination     |
|             | is valid (as defined in sections \ref{sectionVerification}, \ref{sectionPlausibility}).             |
|             | 4) _Victor_ checks if the presented passport matches the attendee       |
|             | by matching the picture to the person.                                  |
|             | Iff conditions 3 and 4 match, the VC is accepted as valid               |
| Actors      | Charlie, Victor                                                         |

: Functional Requirement #05

### \label{sectionF06} F06 Vaccination Certificate Renewal
| F06         | Vaccination Certificate renewal.                                 |
|:------------|:-----------------------------------------------------------------|
| Scenario    | _Alice_ renews _Charlie_'s VC.                                   |
| Description | Passports in Switzerland have a valid lifetime of 10 years.      |
|             | Thus, when _Charlie_'s passport that is associated with his VC   |
|             | runs out, _Charlie_ takes his old and new passport to _Alice_.   |
|             | _Alice_ verifies the validity of the VC against the old passport |
|             | and then re-issues the VC with the new passport as the           |
|             | patient identifier.                                              |
| Actors      | Alice, Charlie                                                   |

: Functional Requirement #06

\newpage

## Non-Functional Requirements
| ID  | Description                                                               |
|-----|---------------------------------------------------------------------------|
| Q01 | The VC of _Charlie_ cannot be forged by _Mallory_                         |
| Q02 | The VC is verifiable by _Victor_ without requiring an internet connection |
| Q03 | The VC of _Charlie_ is anonymous, unless combined with his passport.      |
| Q04 | _Charlie_'s data remains only between _Alice_ and _Charlie_.              |
| Q05 | _Victor_ does not need to perform manual checks to verify _Charlie_'s VC. |

: Non-Functional Requirements

### Q01 Non-Falsifiability
| Q01         | Non-Falsifiability                                                                 |
|:------------|:-----------------------------------------------------------------------------------|
| Scenario    | The VC of _Charlie_ cannot be forged by _Mallory_                                  |
| Description | The data on _Charlie_'s VC is signed by _Alice_ and with an invalid signature the  |
|             | VC is not valid and cannot be replicated by _Mallory_. If  _Mallory_ tries to copy |
|             | the VC of _Charlie_ she will not be able to show a matching passport to  _Victor_. |
| Actors      | Alice, Charlie, Mallory, Victor                                                    |

: Non-Functional Requirement #01

### Q02 Availability and Privacy
| Q02         | Availability and Privacy                                                          |
|:------------|:----------------------------------------------------------------------------------|
| Scenario    | The VC is verifiable by _Victor_ without requiring an internet connection         |
| Description | All information for verifying a VC is contained in the VC. The trust root         |
|             | certificate needs to be  obtained by _Victor_ prior to the verification process.  |
|             | Thus no data of _Charlie_ needs to  sent to a central entity for verification.    |
| Actors      | Charlie, Victor                                                                   |

: Non-Functional Requirement #02

\newpage

### Q03 Anonymity
| Q03         | Anonymity                                                                       |
|:------------|:--------------------------------------------------------------------------------|
| Scenario    | The VC of _Charlie_ is anonymous, unless combined with his passport.            |
| Description | No personal data of _Charlie_ is stored in the VC. _Mike_ can only access the   |
|             | passport  document number and vaccination information after obtaining the       |
|             | VC of _Charlie_.                                                                |
| Actors      | Charlie, Mike

: Non-Functional Requirement #03

### Q04 Privacy
| Q04         | Privacy                                                                       |
|:------------|:------------------------------------------------------------------------------|
| Scenario    | _Charlie_'s data remains only between _Alice_ and _Charlie_.                  |
| Description | As no personal data of _Charlie_ is stored in the VC, only _Alice_ knows the |
|             | data of _Charlie_                                                             |
| Actors      | Alice, Charlie 

: Non-Functional Requirement #04

### Q05 Usability
| Q05         | Usability                                                                          |
|:------------|:-----------------------------------------------------------------------------------|
| Scenario    | _Victor_ does not need to perform manual checks to verify _Charlie_'s VC.          |
| Description | _Victor_ only needs to scan the VC and passport of _Charlie_ to verify _Charlie_'s |
|             | VC. The only manual check is ensuring the passport presented by _Charlie_ matches him.|
| Actors      | Charlie, Victor                                                                    |

: Non-Functional Requirement #05

# Specification
The vaccination certificate is designed to be transmitted optically
using 2D bar codes.
In our application we use QR-Codes with error correction Level 3 (M).
The payload of the QR-Code must be formatted according to Section \ref{sectionFormat}.

## Format
\label{sectionFormat}
The format for the Vaccination Certificate is specified using the following schemas.
Further encoding rules are also specified here.
The schemas are split into a _container_ format
which is described in Section \ref{apx:container-schema} (schema)
and Section \ref{apx:container-example} (example),
and a _data_ format
which is described in Section \ref{apx:data-schema} (schema)
and Section \ref{apx:data-example} (example),

While the format is specified as a JSON-Schema,
it must store binary data, which JSON does not support.
We use the CBOR format (Concise Binary Object Representation),
and only use JSON-Schemas in order to specify the fields
that must be included.

JSON could be used, by encoding all binary values as Base64,
but this has several downsides, as described in section \ref{sectionDataEncoding}.

### Data
The data follow the FHIR Standard for Immunization Resources [@fhirImmunization].
We chose to use the value sets proposed by the EU for their Digital COVID Certificate,
as they support all the necessary values for our operation as well.

### \label{sectionDataEncoding} Data Encoding
The CBOR encoding has several advantages.
The most important one is that no matter the field order on the input data,
CBOR results in a well defined output.
This is useful to make signature verification easier,
because we don't need to worry about the input formatting.
As long as it matches the schema and the content is the same,
the signatures must also match.

Another advantage that it has over i.e. JSON is that it supports binary data
directly, which is useful for storing cryptographic keys and signatures.

The third advantage is that it compresses the input data quite a bit,
although we do compress the CBOR binary stream again for a roughly 15%
savings in data size.

Although our Vaccination Certificate is one to two kilobytes in size when
fully expanded, only 750 bytes must be written to the QR-Code,
making it easier to scan.

\newpage

The `issuerKey` is encoded as DER (Distinguished Encoding Rules).
This binary stream is then signed by the trust root. DER is also used
as a transport encoding in the VC.

The `data` is encoded using CBOR.
This binary stream is then signed by the issuer,
but it is not used for transporting the `data`.
Instead, the data are included as a nested structure in the container.
The entire container is then CBOR encoded, compressed and written to the QR-Code.

### Pseudo Code
The following pseudo code describes the VC creation process:

```{.python .numberLines}
data = load_immunization_record()

data_cbor = dumps_cbor(data)

sk = load_issuer_key()
signature = sk.sign(data_cbor, hashfunc=sha256)

container = {
    'data': data
    'dataSignature': signature,
    'issuerKey': load_issuer_public_key().to_der(),
    'issuerKeySignature': load_issuer_public_key_signature(),
    'trustRootIdentifier': load_trust_root_key_identifier())
}

container_cbor = dumps_cbor(container)
qr_contents = compress(container_cbor)
```

\newpage

## Signing
The VC contains multiple signatures to ensure its integrity.
In order to ensure good scaling, while also providing distributed,
offline creation and verification of VCs, we chose the following approach:

There exists one (or a small number) of trust roots.
The verifiers only need to know and trust these trust roots.
A trust root signs the public keys of a VC-Issuer (akin to a PKI).

The VC-Issuers use their signed key pairs to sign VCs and include their
public key, the signature by the trust root, and a trust-root identifier
in the VC.

We use Elliptic Curve keys to sign the VCs due to the smaller key sizes
(while providing equivalent security) because we need to transport
keys in a QR-Code.

![Contents of a VC](assets/vc_contents.png){width=40%}

## \label{sectionVerification} Verification
The verification flow is also described in [@fig:vc-verification].
In order to verify a VC, we first find the trust root used by comparing  the
`trustRootIdentifer` to our known trust roots,
then check if the `issuerKeySignature` matches the `issuerKey` and was made
by that trust root.

If that matches, check if the `dataSignature` matches the `data` (in the
CBOR format) and was made by the key pair that the `issuerKey` belongs to.
If both signatures match, the certificate was made by a trusted issuer
and not edited.

The next step is a plausibility step, which is described
in section \ref{sectionPlausibility}.

The Verifier must also compare the Patient Identifier of the VC to a
legal identity document provided by the Certificate Owner and
ensure that the person presenting the VC matches the identity document.

\newpage

![Verification Flow](assets/vc_verification.png){height=90% #fig:vc-verification}

\newpage

## \label{sectionPlausibility} Plausibility
Assuming that the signatures are all valid
and thus the chain of trust unbroken,
there are some plausibility checks that can be done on the `data`.
There should not be any VCs issued with implausible data,
but this check can help catch VCs issued in error or maliciously.
The Verifier should perform these checks anyway,
and this automated check helps avoid fatigue.

The following should be true for a plausible VC:

- The `doseNumber` and `seriesDoses` are equal.
- The `vaccineCode`, `manufacturer`, and `targetDisease` match each other and
  the value sets.
- The `recorded` date is in the past
- The `occurrenceDateTime` is $\leq$ the `recorded` date.
- The `occurrenceDateTime` is 10 days in the past.

## Key Distribution
The key distribution is handled by the HIN, as every healthcare professional
already has a signed key pair that can be used.

In case any key pair gets compromised, it can be put on a CRL.
The affected Certificate Owners must then be issued a new VC,
similar to F06 (Section \ref{sectionF06}).

# Comparison to other solutions
Beside developing our own solution,
we are making some comparisons to other proposed or existing solutions to
the problem of verifying that a person is vaccinated.
We only compare the EU's [Digital COVID Certificate](https://ec.europa.eu/health/ehealth/covid-19_en) (DCC)
and Switzerland's [CovidCertificate](https://github.com/admin-ch/CovidCertificate-Documents) (CC)
solutions in detail because they are both publicly documented and regionally relevant to the authors.

## EU Solution
### Use Cases
One of the big differences to our VC is that the DCC supports not only

- proof of vaccination, but also
- proof of a negative COVID-test and
- proof of recovery from COVID-19

Through a change in the "`data`"-Portion of our VC it should be
possible to also enable support for the other two proofs.

### Data Format
In an earlier document the EC recommended FHIR as a data exchange format for
the DCC [@trustFrameworkInterop].
This has since been abandoned in favor of a simplified structure.
However, the DCC still retains the Value Sets of FHIR for disease and prophylaxis identification.
Our VC retains the use of FHIR,
specifically the [Immunization](https://www.hl7.org/fhir/immunization.html)
record for easier interoperability with existing health information systems.

### Identification
The DCC contains personally identifying information as a non-optional component.
This includes the name and date of birth of the carrier (or "subject")
^[in our system called the _Certificate Owner_] of a DCC.
A tie to an internationally usable form of identification,
such as a passport or a national ID-Card is however optional.

Our VC approach only contains the passport number of the Certificate Owner,
meaning that without the corresponding passport it cannot be tied
to a specific person.

Each DCC also contains a Unique Vaccination Certificate Identifier (UVCI),
which allows it to be uniquely tied to data sets in other systems.
This means that it can act as a digital immunization card, usable in medical settings
^[combined with the proper health information systems].

\newpage

### Cryptography
Both the DCC and our VC rely on similar PKI-Cryptography in order to
ensure authenticity and integrity.
Implementors of the DCC must provide a list of public keys which are authorized
to sign DCCs.
This list must be known by the verifiers and used to check the integrity of a DCC.

Our solution also has a list of Trust Roots ("Bob"),
but those keys are not used directly to sign VCs.
They are used to authorize issuers ("Alice") which then sign VCs using their own key pair.
The VC contains (as explained in section \ref{sectionFormat})
all the information needed to verify it, except the Trust Roots.
This significantly enlarges our VC,
but means that it is scalable to many issuers,
which is suitable for our decentralized approach.

It also has the advantage that, should an issuers key pair be compromised,
the VCs signed by them can be declared invalid.
Invalidated VCs must the be renewed following the same process as initial acquire.

\newpage

## Swiss Solution
The Swiss CovidCertificate is designed to be interoperable with the EU's DCC.
This necessarily means that Switzerland is essentially a "member state"
implementing the DCC, even though it is not an EU member state.

As of June 2021 it is technically possible for the EU to recognize Swiss
CovidCertificates, but there is no legal basis yet.

The Swiss solution is well documented and the code is available
[publicly](https://github.com/admin-ch/CovidCertificate-Documents).

### Certificate Service
The Swiss federal administration provides an online service and an API in order
to generate CovidCertificates.
This means that the signing takes place centrally, with a single key pair.

This online service is only available to authorized users,
defined by the cantons, which use various forms
^[i.e. HIN Login, CH-Login (eGovernment), FED-LOGIN (using Smart cards)] of secure login
to access the platform.

This has the advantage of not exposing the issuer keys,
because the only key is kept inside a central signing service,
but comes with the disadvantage of needing to be online to issue CovidCertificates.
It also has obvious privacy implications,
because all the vaccination data passes through the central server
of the Swiss government.

The online service consists of a Web-GUI where authorized users
can enter the vaccination data into a form,
and receive the CovidCertificate in the end.
There are also various plausibility checks to minimize user error.

The offered API is used by Vaccination Information Systems
(such as [vacme.ch](https://blog.vacme.ch/be/)), which are used by the cantons
to organize vaccination efforts, to automatically issue certificates
when a person receives their final vaccination.

# User Interface
This section will introduce some mockups to show how the planned
user interface for the issuance and verification processes should look
like. It is split into the issuance side and the verification side.

## Issuance
For issuing a VC the issuer first needs to enter /
select the needed vaccination data that should be contained in the VC.
They also need to enter the ID document number of the Certificate Owner.
If the data is valid it will generate a VC with the entered data.

![Data Entry](./assets/mockups/issuance/data_entry.png)

\newpage
Upon issuing the VC, it will be displayed on the screen and the issuer can print it out and hand the VC to the Certificate Owner.
Alternatively, the Certificate Owner can scan the QR-Code and reproduce it themselves.

![Certificate Display](./assets/mockups/issuance/certificate_display.png)

\newpage
### Sequence Diagram
The following sequence diagram visualizes the flow for the issuance process.

![Sequence Diagram - Issuance](assets/sequence_diagrams/issuance.png)

\newpage
## Verification
For the verification the Verifier first needs to scan the QR code of the VC they want to verify.

![Scanning VC](./assets/mockups/verification/1.png)

\newpage
Next they need to scan the ID document associated with the VC.
This is accomplished using the machine readable zone (MRZ)
on an ICAO Document 9303 [@doc9303] compliant passport or ID-card.
They also have the option to go back and scan a different VC.

![Scanning ID document](./assets/mockups/verification/2.png)

\newpage
Finally the Verifier will be displayed the contents of the VC if
it's valid and is prompted to verify that the ID document belongs
to the Certificate Owner.
Then they can either go back and scan a different ID document
or start a new verification process.

![Verification Results](./assets/mockups/verification/3.png)

\newpage
### Sequence Diagram
The following sequence diagram visualizes the flow for the verification process.

![Sequence Diagram - Verification](assets/sequence_diagrams/verification.png)

# Appendix
## \label{apx:container-schema} Container Schema
[container-schema.json](https://gitlab.com/martyschaer/project-2/-/raw/master/specification/container-schema.json)
```{.json}
{
    "$schema": "http://json-schema.org/draft/2020-12/schema",
    "$id": "bfh.ch/p2/kades2-scham17/vc-container",
    "title": "Vaccination Certificate",
    "description": "A non-forgible vaccination certificate",
    "type": "object",
    "properties": {
        "data": {
            "description": "The Immunization entry, as specified in data-schema.json",
            "$ref": "bfh.ch/p2/kades2-scham17/vc-data"
        },
        "issuerKey": {
            "type": "string",
            "description": "Public key that signed this certificate."
        },
        "dataSignature": {
            "type": "string",
            "description": "Signature of the 'data' field by the issuer."
        },
        "issuerKeySignature": {
            "type": "string",
            "description": "Signature of the 'issuerKey' by the trustRoot."
        },
        "trustRootIdentifier": {
            "type": "string",
            "description": "SHA1 hash (hex format) of the trustRoot public key."
        }
    }
}
```

\newpage

## \label{apx:container-example} Container Example
[container-example.json](https://gitlab.com/martyschaer/project-2/-/raw/master/specification/container-example.json)
```{.json .numberLines}
{
  "data": "see data-example.json",
  "dataSignature": "(binary data omitted)",
  "issuerKey": "(binary data omitted)",
  "issuerKeySignature": "(binary data omitted)",
  "trustRootIdentifier": "9d4c09ff15622043091bebeff9d441cc962a9032"
}
```

\newpage

## \label{apx:data-schema} Data Schema
[data-schema.json](https://gitlab.com/martyschaer/project-2/-/raw/master/specification/data-schema.json)
```{.json .numberLines}
{
    "$schema": "http://json-schema.org/draft/2020-12/schema",
    "$id": "bfh.ch/p2/kades2-scham17/vc-data",
    "title": "Vaccination Certificate",
    "description": "A subset of a FHIR Immunization resource.",
    "type": "object",
    "properties": {
        "status": {
            "description": "The status of the vaccination.",
            "type": "string",
            "enum": [
                "completed",
                "entered-in-error",
                "not-done"
            ]
        },
        "resourceType": {
            "description": "Identifies the type of FHIR resource.",
            "type": "string",
            "enum": [ "Immunization" ]
        },
        "vaccineCode": {
            "description": "Which vaccine was administered. Must match with manufacturer",
            "type": "object",
            "properties": {
                "coding": {
                    "description": "Describes the system and ID of the vaccine.",
                    "type": "array",
                    "maxItems": 1,
                    "items": {
                        "type": "object",
                        "properties": {
                            "system": {
                                "type": "string",
                                "enum": [
                                  "https://ec.europa.eu/health/documents/community-register/html/"
                                ]
                            },
                            "code": {
                                "description": "Any code allowed by the system property. Must match the text.",
                                "type": "string"
                            }
                        }
                    }
                },
                "text": {
                    "description": "A human readable description of the coding-value",
                    "type": "string"
                }
            }
        },
        "patient": {
            "description": "Identifies the subject of the vaccination certificate",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [
                        "Patient"
                    ]
                },
                "identifier": {
                    "type": "object",
                    "properties": {
                        "value": {
                            "description": "A passport/id document number.",
                            "type": "string"
                        }
                    }
                }
            }
        },
        "recorded": {
            "description": "When was this vaccination certificate issued (ISO-8601).",
            "type": "string",
            "format": "date"
        },
        "occurrenceDateTime": {
            "description": "When was the vaccine administered (ISO-8601).",
            "type": "string",
            "format": "date"
        },
        "manufacturer": {
            "description": "Who manufactured the vaccine. Must match with vaccineCode.",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [ "Organization" ]
                },
                "identifier": {
                    "type": "object",
                    "properties": {
                        "value": {
                            "description": "A manufacturer identifier according to https://spor.ema.europa.eu/v1/organisations",
                            "type": "string"
                        }
                    }
                },
                "display": {
                    "description": "A human readable manufacturer name",
                    "type": "string"
                }
            }
        },
        "lotNumber": {
            "description": "The lot number of the administered vaccine",
            "type": "string"
        },
        "protocolApplied": {
            "description": "Describes the system and ID of the vaccine.",
            "type": "array",
            "maxItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "doseNumberPositiveInt": {
                        "description": "The number of doses that were administered already.",
                        "type": "number",
                        "minimum": 1
                    },
                    "seriesDosesPositiveInt": {
                        "description": "The number of doses the the subject of the certificate should receive.",
                        "type": "number",
                        "minimum": 1
                    },
                    "targetDisease": {
                        "type": "array",
                        "maxItems": 1,
                        "items": {
                            "coding": {
                                "description": "Describes the disease that the vaccine is targeting (COVID-19).",
                                "type": "array",
                                "maxItems": 1,
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "system": {
                                            "type": "string",
                                            "enum": [
                                                "http://snomed.info/sct"
                                            ]
                                        },
                                        "code": {
                                            "description": "The code for COVID-19 as described by the system",
                                            "type": "string",
                                            "enum": [
                                                "840539006"
                                            ]
                                        }
                                    }
                                }
                            },
                            "text": {
                                "type": "string",
                                "enum": [
                                    "COVID-19"
                                ]
                            }
                        }
                    }
                }
            }
        }
    }
}
```

\newpage

## \label{apx:data-example} Data Example
[data-example.json](https://gitlab.com/martyschaer/project-2/-/raw/master/specification/data-example.json)
```{.json .numberLines}
{
  "status": "completed",
  "resourceType": "Immunization",
  "vaccineCode": {
    "coding": [{
        "system": "https://ec.europa.eu/health/documents/community-register/html/",
        "code": "EU/1/20/1507"
      }],
    "text": "COVID-19 Vaccine Moderna"
  },
  "patient": {
    "type": "Patient",
    "identifier": {
      "value": "S00004156"
    }
  },
  "recorded": "2021-06-16",
  "occurrenceDateTime": "2021-06-03",
  "manufacturer": {
    "type": "Organization",
    "identifier": {
      "value": "ORG-100031184"
    },
    "display": "Moderna Biotech Spain, S.L."
  },
  "lotNumber": "3002541",
  "protocolApplied": [{
      "doseNumberPositiveInt": 2,
      "seriesDosesPositiveInt": 2,
      "targetDisease": [{
          "coding": [{
              "system": "http://snomed.info/sct",
              "code": "840539006"
            }],
          "text": "COVID-19"
        }]
    }]
}
```

# Sources
::: {#refs}
:::

