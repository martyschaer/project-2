import datetime

from immunization import Immunization
from coding import VaccineCode, Manufacturer
from certificate import VaccineCertificate
from zlib import compress, decompress
from validator import is_signature_valid
from json import dumps, loads
from cbor2 import dumps as cdumps
from cbor2 import loads as cloads
from base64 import b64encode, b64decode
import qrcode


def create_data() -> Immunization:
    return Immunization(
        patient_id='S00004156',
        vaccine_code=VaccineCode.MODERNA,
        manufacturer=Manufacturer.MODERNA,
        lot_number='3002541',
        occurrence=datetime.date(2021, 6, 3),
    )


def encode(certificate: VaccineCertificate) -> bytes:
    return compress(cdumps(certificate.sign()))
    #return cdumps(certificate.sign())
   #signed = dumps(certificate.sign())
   #print('Signed Certificate:', signed)
   #compressed = b64encode(compress(signed.encode('utf-8')))
   #return compressed

def decode(qr_contents: bytes) -> dict:
    #json = decompress(b64decode(qr_contents)).decode('utf-8')
    #return loads(json)
    return cloads(decompress(qr_contents))
    #return cloads(qr_contents)


def save_qr_code(payload: bytes, file_name: str):
    qr = qrcode.QRCode(error_correction=qrcode.ERROR_CORRECT_Q)
    qr.add_data(payload)
    img = qr.make_image(fill_color='black', back_color='white')
    img.save(file_name)


def main():
    data = create_data()
    print('Data created: ', data.to_json())
    vaccination_certificate = VaccineCertificate(data)
    qr_contents = encode(vaccination_certificate)
    save_qr_code(qr_contents, 'result/certificate.png')
    print('Data written to QR-Code ({} bytes): {}'.format(len(qr_contents), qr_contents))

    cert = decode(qr_contents)
    print('Loaded from QR-Code ({} bytes): {}'.format(len(str(cert)), str(cert)))
    valid = is_signature_valid(cert)
    if valid:
        print('Signatures are all valid!')
    else:
        print('Invalid signatures.')

if __name__ == '__main__':
    main()
