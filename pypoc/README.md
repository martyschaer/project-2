How to setup (first time):

```
# create and activate venv
python -m venv .venv
source .venv/bin/activate

# install dependencies
python -m pip install -r requirements.txt

# deactivate the venv (after you're done)
deactivate
```

How to use:
```
# Generate new keys
python keygen.py

# Run the example
python main.py
```

