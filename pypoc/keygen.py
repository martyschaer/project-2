from ecdsa import SigningKey
from ecdsa.curves import BRAINPOOLP256r1
from hashlib import sha1, sha256
from base64 import b64encode

curve_to_use = BRAINPOOLP256r1
hashfunc_to_use = sha256

def main():
    trust_root_key = SigningKey.generate(curve=curve_to_use, hashfunc=hashfunc_to_use)
    with(open('key/trustRootKey.base64', 'w')) as f:
        f.write(b64encode(trust_root_key.to_der()).decode('utf-8'))

    trust_root_key_identifier = sha1(trust_root_key.get_verifying_key().to_string()).hexdigest()
    with(open('key/trustRootKeyIdentifier.hex', 'w')) as f:
        f.write(trust_root_key_identifier)

    issuer_key = SigningKey.generate(curve=curve_to_use, hashfunc=hashfunc_to_use)
    with(open('key/issuerKey.base64', 'w')) as f:
        f.write(b64encode(issuer_key.to_der()).decode('utf-8'))

    issuer_public_key = issuer_key.get_verifying_key()
    issuer_public_key_b64 = b64encode(issuer_public_key.to_der())
    with(open('key/issuerPublicKey.base64', 'w')) as f:
        f.write(issuer_public_key_b64.decode('utf-8'))

    issuer_public_key_signature = b64encode(trust_root_key.sign(issuer_public_key_b64))
    with(open('key/issuerPublicKeySignature.base64', 'w')) as f:
        f.write(issuer_public_key_signature.decode('utf-8'))

if __name__ == '__main__':
    main()
