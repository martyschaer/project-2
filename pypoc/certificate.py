from hashlib import sha256

from cryptoloader import *
from immunization import Immunization
from cbor2 import dumps as dumps_cbor
from base64 import b64encode, b64decode
from zlib import compress


class VaccineCertificate:
    def __init__(self,
                 data: Immunization,
                 ):
        self.data = data

    def sign(self) -> {}:
        data_cbor = self.encode_data()
        sk = load_issuer_key()
        signature = sk.sign(data_cbor, hashfunc=sha256)
        return {
            'data': self.data.to_dict(),
            'dataSignature': signature,
            'issuerKey': b64decode(load_issuer_public_key()),
            'issuerKeySignature': b64decode(load_issuer_public_key_signature()),
            'trustRootIdentifier': b64decode(load_trust_root_key_identifier())
        }

    def encode_data(self) -> bytes:
        return dumps_cbor(self.data.to_dict())

