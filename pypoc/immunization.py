from coding import Status, VaccineCode, Manufacturer, Protocol
import json as json_encode
from datetime import date


class Immunization:
    def __init__(self,
                 patient_id: str,
                 vaccine_code: VaccineCode,
                 manufacturer: Manufacturer,
                 lot_number: str,
                 recorded: date = date.today(),
                 occurrence: date = date.today(),
                 status: Status = Status.COMPLETED,
                 protocol: Protocol = Protocol(2, 2),
                 ):
        self.status = status
        self.vaccine_code = vaccine_code
        self.patient_id = patient_id
        self.recorded = recorded
        self.occurrence = occurrence
        self.manufacturer = manufacturer
        self.lot_number = lot_number
        self.protocol = protocol

    def to_json(self) -> str:
        json = dict()
        json['status'] = self.status.value
        json['resourceType'] = 'Immunization'
        json['vaccineCode'] = self.vaccine_code.to_dict()
        json['patient'] = {
            'type': 'Patient',
            'identifier': {
                'value': self.patient_id
            }
        }
        json['recorded'] = self.recorded.isoformat()
        json['occurrenceDateTime'] = self.occurrence.isoformat()
        json['manufacturer'] = self.manufacturer.to_dict()
        json['lotNumber'] = self.lot_number
        json['protocolApplied'] = self.protocol.to_dict()

        return json_encode.dumps(json)

    def to_dict(self) -> {}:
        return json_encode.loads(self.to_json())
