from zlib import decompress

from ecdsa import VerifyingKey, SigningKey
from cryptoloader import load_trust_root_key
from base64 import b64encode, b64decode
from hashlib import sha256
from cbor2 import dumps


def is_signature_valid(cert: dict) -> bool:
    # load trust root public key from file
    trust_root_publickey = load_trust_root_key()
    trust_root = SigningKey.from_der(b64decode(trust_root_publickey), hashfunc=sha256).get_verifying_key()

    # load the issuer key and signature from the certificate
    issuer_keyb64 = cert['issuerKey']
    issuer_key = VerifyingKey.from_der(issuer_keyb64, hashfunc=sha256)
    issuer_key_signature = cert['issuerKeySignature']

    # verify the issuer key
    if trust_root.verify(issuer_key_signature, b64encode(issuer_keyb64), sha256):
        print('Issuer key signature is valid.')
    else:
        print('Failed to verify issuer key signature.')
        return False

    # load the data and signature from the certificate
    data = cert['data']
    data_signature = cert['dataSignature']

    # verify the data
    if issuer_key.verify(data_signature, dumps(data), sha256):
        print('Data signature is valid.')
    else:
        print('Failed to verify data signature.')
        return False

    return True
