from base64 import b64decode

from ecdsa import SigningKey


def load_issuer_key() -> SigningKey:
    with(open('key/issuerKey.base64', 'r')) as kf:
        sk = SigningKey.from_der(b64decode(kf.readline()))
        return sk


def load_issuer_public_key() -> str:
    with(open('key/issuerPublicKey.base64', 'r')) as pkf:
        return pkf.readline()


def load_issuer_public_key_signature() -> str:
    with(open('key/issuerPublicKeySignature.base64', 'r')) as pksf:
        return pksf.readline()


def load_trust_root_key() -> str:
    with(open('key/trustRootKey.base64', 'r')) as trk:
        return trk.readline()


def load_trust_root_key_identifier() -> str:
    with(open('key/trustRootKeyIdentifier.hex', 'r')) as trkidf:
        return trkidf.readline()
