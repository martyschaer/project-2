from enum import Enum


class Coding:
    def __init__(self, system: str, code: str):
        self.system = system
        self.code = code


class VaccineCode(Coding):
    def __init__(self, code: str, text: str):
        super().__init__('https://ec.europa.eu/health/documents/community-register/html/', code)
        self.text = text

    def to_dict(self) -> {}:
        return {
            'coding': [{
                'system': self.system,
                'code': self.code
            }],
            'text': self.text
        }


VaccineCode.PFIZER = VaccineCode('EU/1/20/1528', 'COMIRNATY')
VaccineCode.MODERNA = VaccineCode('EU/1/20/1507', 'COVID-19 Vaccine Moderna')
VaccineCode.JANSSEN = VaccineCode('EU/1/21/1525', 'COVID-19 Vaccine Janssen')


class TargetDisease(Coding):
    def __init__(self, code: str, text: str):
        super().__init__('http://snomed.info/sct', code)
        self.text = text

    def to_dict(self) -> {}:
        return {
            'coding': [{
                'system': self.system,
                'code': self.code
            }],
            'text': self.text}


TargetDisease.COVID19 = TargetDisease('840539006', 'COVID-19')


class Manufacturer(Coding):
    def __init__(self, code: str, name: str):
        super().__init__('https://spor.ema.europa.eu/v1/organisations', code)
        self.name = name

    def to_dict(self) -> {}:
        return {
            'type': 'Organization',
            'identifier': {
                'value': self.code
            },
            'display': self.name
        }


Manufacturer.BIONTECH= Manufacturer('ORG-100030215', 'BioNTech Manufacturing GmbH')
Manufacturer.MODERNA = Manufacturer('ORG-100031184', 'Moderna Biotech Spain, S.L.')
Manufacturer.JANSSEN = Manufacturer('ORG-100001417', 'Janssen-Cilag International NV')


class Protocol:
    def __init__(self, dose_number: int, series_doses: int):
        self.dose_number = dose_number
        self.series_doses = series_doses

    def to_dict(self) -> []:
        return [{
            'doseNumberPositiveInt': self.dose_number,
            'seriesDosesPositiveInt': self.series_doses,
            'targetDisease': [TargetDisease.COVID19.to_dict()]
        }]


class Status(Enum):
    COMPLETED = 'completed'
    ERROR = 'entered-in-error'
    NOT_DONE = 'not-done'
