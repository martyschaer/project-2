---
title: 'Project 2'
subtitle: 'Work Journal'
author:
  - Severin Kaderli
  - Marius Schär
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lang: "en-US"
toc: true
titlepage: false
rule-color: 4caf50
link-color: 4caf50
links-as-notes: true
...

# Work Journal
This document contains the work journal for _Project 2 - Digital Vaccine Passport_.

# Prior
- Set up a repository on [GitLab](https://gitlab.com/martyschaer/project-2)
- Setup documents with citations, glossary, etc.

# 2021-03-11
- Meeting with advisors about the topic
  * Notes from each meeting can be found in [`NOTES.md`](https://gitlab.com/martyschaer/project-2/-/blob/master/NOTES.md)
- Discuss further working in the team

# 2021-03-16
- Clean up the document compilation system
- Find existing research/advances in the field of vaccine passports
- Create schedule for the project

# 2021-03-25
- Create and play around with a prototype using OpenSSL

# 2021-03-30
- Improve application of compression in PoC scripts

# 2021-03-31
- Work on Personas and Requirements
- Flesh out technical details
- Research ICAO 9303
- Discuss/Discover possible attacks on the system

# 2021-04-05
- Discuss other implementations
- Work on Use-Cases

# 2021-04-13
- Divide workload
- Work on personas

# 2021-04-14
- Work on functional requirements

# 2021-04-15
- Work on Non-Functional requirements
- Work on document layout
- Research FHIR
- Meeting with advisors

# 2021-04-26
- Meeting with advisors with the following TODOs:
  * Read up more on EU documents
  * Compare EU solution to ours
  * Spec. for how to sign a VC
  * What makes a consistent VC?
  * Should inconsistencies be reported and investigated (at VC issuer level)?
  * How does a verifier deal with inconsistencies? (barred from entry, quick test, ...)

# 2021-05-04
- Divy up work to write prose
- Decide VC spec
- Coordinate further work

# 2021-06-06
- Create mockups
- Create sequence diagrams

# 2021-06-16
- Work on article

# 2021-06-17
- Work on article

# 2021-06-18
- Finalize article
- Hand in article

# 2021-06-25
- Work on presentation

# 2021-06-27
- Work on presentation
