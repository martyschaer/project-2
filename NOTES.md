---
title: 'Notes'
subtitle: 'Project 2 - Digital Vaccine Passport'
author:
  - Severin Kaderli
  - Marius Schär
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lang: "en-US"
toc: false
lot: false
titlepage: false
rule-color: 4caf50
link-color: 4caf50
links-as-notes: true
...

# Meetings
## 2021-03-11 Kick Off
- Idea: 
  * Impfprivilegien
  * Test nachweisen
  * Digital, aber ausdruckbar
  * 3 Things: Who am I, Ordentliche Impfung, der Impfstoff wurde verabreicht
  * E-Reisepass  
  * Health Information Network (international?)
    + Digital Signature, Blockchain, Artzzeugnis
    + Working Blackbox
    + Urs Fischer (Contact by Laube)
  * Zweite Impfung setzt erste voraus (contains)
    + Verbreitung Kartenpässe
    + Entweder ID oder Reisepass
- P2
  * Processes, Crypto, Handhabung
- TBD:
  * Projektplan
  * Termin 22. March Teams Meeting
  * Research
- Abgabeobjekte:
  * Bericht: 2021-06-18
  * Presentation: 
  * Arbeitsjournal
- Bewertung:
  * same as bachelor
  * Recv. by Email
- Umsetzung in Bachelor-Arbeit
  * Alle Fragen geklärt

# 2021-04-13
- Naming
  * Vaccine Passport -> Vaccination Certificate (VC)
  * Patient -> Certificate Owner (Gian Aerni)
  * Doctor -> Issuer (Vroni Moosheer)
- Attack Scenarios
  * Integrity
    + Visit event without being vaccinated
    + Travel to another country without being vaccinated
  * Privacy
    + Find out who is vaccinated
    + Track people that are using the VC
  * Availability
    + Disrupt verification of VCs (Denial of Service)
- Verification
  * What is a valid VC
    + Signature must be valid and chain of trust valid
    + Date of Certification in the past
    + Date of Vaccination in the past
    + 'Number' of vaccination is correct
    + Passport Document Number must match presented passport
    + Presented passport must match alleged Certificate Owner (Picture)
  * How to deal with false negative (Verifier cannot verify valid VC)
- Attacks/Risks of Passport Document Number being public
  * Presumably little impact

# Personas
- Issuer
  * Certify that vaccination occured
- CO
  * Attend events
  * Travel internationally
  * Personal data remains private
  * VC does not _require_ smartphone
    + Paper version is available
- Verifier
  * Quickly and easily verify a person has a valid passport
    + Needed for verification: vaccine passport + a valid international id (travel passport)
- Attacker
  * Generate valid, but fake VC
  * Generate valid looking (but invalid) fake VC
  * Copy someone else's VC
  * Steal personal data of __CO__

# Scoping
- Political implications
- Only vaccination, excluding negative test results / COVID-19 recovery
- Dishonest behaviour of __Issuer__ or __Verifier__ is excluded.

# Approach to the problem
- A Certificate Authority (HIN/BAG) issues certificates to __Issuer__ s
- __Issuer__ digitally-signs of the information on the vaccine passport with his certificate
  * The information + signature is written to a QR-Code on the __Issuer__-Device
- __CO__ scans the QR-Code from the __Issuer__ to copy it or receives a print-out
- __Verifier__ downloads the CA-Cert as a root of trust & the Certificate Revocation List
- __Verifier__ scans the QR code of __CO__ and verifies it against the signature

- Only the ID-Document Number is stored as Personally Identifying Information in the QR-Code
- The ID-Document Number is hashed, to keep it secret
- The passport number can be read from the MRZ of a ICAO Document 9303 compliant id,
  or entered by hand
- The passport number can be verified to be correct by reading it by RFID

- Information in the QR Code:
  * ID-Document Number (Irreversible(?))
  * Payload
    + Illness
    + Vaccine Manufacturer
    + Number of Vaccination
    + Site of Vaccination
    + Date of Vaccination
    + Date of Certification
  * Signature of the Document Number + Payload (by _Issuer_)
  * Public Key of _Issuer_
  * Signature of Public Key (by _CA_)

Attack Scenarios:

  - Forging of ID-Document (Out of Scope)
  - Compromise of Root-CA Private Key (Out of Scope)
  - Compromise of _Issuer_ Private Key
    * Certificate Revocation Lists (CRL) -> Denial of Service
  - Copying someone else' VC
    * Impossible by design
  - Malicious Behaviour by _Issuer_ and _Verifier_ (Out of Scope)
  - Issuing Self-Signed VC
    * Impossible by design (Signature does not match Root-CA)
  - $k$-Collision when signing

# Other approaches
- Israel
  * VC & Personal ID
  * VCs can apparently be easily faked
- Apparently ICAO and the WHO are also developing a solution

## EU Solution
- Obtaining a certificate
  * Charlie is identified at the time of vaccination
  * Vaccination data is stored in a DB (Immunisation Information System)
  * Certificate is issued at a later date
- A certificate has a Unique Vacc. Certificate Identifier for Online Verification
- Medium Agnostic (not digital-only)
- "Data Minimization"
- Will not only support vaccination, but also negative tests and recovery
- Has additional "Cert. Reader" role, which may extract detailed information
- ID-Binding (to a ICAO 9303 doc) is not required in some scenarios
- Centered around the Global Health Trust Anchor by the WHO, based on the ICAOs Public Key Directory
- List Authorized Health Certificate Issuers maintained by national Public Health Authority
- Country set up a CA to issue key-pairs for signing
- Verifier Refresh-Rate and revocation of health certificates is defined nationally
- Health Certificate Revocation list is published by PHA
- Online verification shall not be made mandatory
- Online verification requests shall be answered within 2 (!) business days
- Distribution of Trust Anchors by secure email or downloaded from PHA website
- Only ISO standard 2D barcodes on "Augmented Paper", Aztec is recommended

## Detailed analysis
- Basic Interoperability Elements:
  * Minimum Dataset
    + Person identification
    + Vaccination information
    + Certificate Metadata
  * Unique Vaccination Certificate identifier (UVCI)
    + Associate additional information with a VC
    + " means to verify the veracity of the VC"
    + no personal data, but a "Unique Primary Key"
  * Trust Framework
- Vaccination information
  * Linked to "preferred EU code systems"
- Medium to Long Term: all data shared through MyHealth@EU
- Differing verification protocols between enhanced paper / purely digital
  * "details must be elaborated"
  * "security analysis and risk assessment should be used"
- Seperate Certificate for medical inability to be vaccinated
- QR Code is recommended to be rendered between 35-60mm on presentation media

### Minimum Dataset 
Fields in _cursive_ are optional.

- Person Identification
  * Name
  * Date of Birth
  * _Person Identifier (Citizen ID/Passport Number)_
- Vaccination Info (multiple possible)
  * Disease/Agent (SNOMED/ICD-10)
  * Vaccine (SNOMED)
  * Vaccine Medical Product (Product Name. e.g. 'COMIRNATY concentrate for dispersion for injection')
  * Vaccine manufacturer (WHO Emergency use listing. e.g. 'BioNTech Manufacturing GmbH')
  * Number in a series of doses (e.g. '1 out of 2 doses')
  * _Lot Number_
  * Date of Vaccination (ISO 8601, without time)
  * Administering Centre
  * _Health Professional Identification_
  * Country of Vaccination (ISO 3166)
  * _Next vaccination date_ (ISO 8601, without time)
- Certificate Metadata
  * Certificate Issuer (who has issued the VC)
  * Certificate Identifier (UVCI)
  * _Valid from_ (ISO 8601, without time)
  * _Valid until_ (ISO 8601, without time)
  * Certificate schema version (Semantic Versioning, currently 1.0.0)

### UVCI
![UVCI Possibilities](assets/eu_uvci_possibilities.png) 

- Uppercase US-ASCII alpha numberical and `r'[/#:]'`
- max-len 27-30
- Version prefix (two digits, currently 01)
- Country prefix (ISO-3166-1 alpha-2)
- Checksum should be used when human transcription is expected
- Checksum must not be used for VC validity
- Checksum should be ISO-7812-1 LUHN-10) and is separated by a #
- UVCIs cannot be recycled

- Option 1
  * separated by '/', no spec. chars within blocks
  * each block defined by member states
  * "enhance possibilities for offline or analogue (!) verification
- Option 2
  * Entirely opaque, unique string
  * Member state is defines generation mechanism
  * no "human interoperability"
- Option 3
  * Like option 2, but the issuing entity is given separately

# FHIR
FHIR (Fast Healthcare Interoperability Resources) is a standard for exchanging healthcare information.
FHIR consists of XML and JSON schemas for all possible resources in healthcare environments. For each resource
type it lists all optional and mandatory fields.

For our project the immunization resource seems the most suited. It describes "the event of a patient being administered
a vaccine".

The following fields seem the most relevant for our application:

* status
* vaccineCode
* patient
* occurence
* manufacturer
* lotNumber
* performer
  * actor
* protocolApplied
  * doseNumber
  * seriesDoses

# CBOR
CBOR (Concise Binary Object Representation) is an extended version of the JSON data model. It is intended to allow
the simple transfer of binary data using a well defined format and with a small size. CBOR can be extended using so
called tags.

# COSE
COSE (CBOR Object Signing and Encryption) extends the CBOR format to allow signature and encryption. The COSE object
structure is built on top of the CBOR array type.

# What makes a valid VC
- All signatures are valid and the chain of trust is unbroken
- Patient identifier matches the presented passport document
- the presented passport document is valid and matches the person
- The doseNumber matches the seriesDoses
- target disease and vaccine code match known values (opt)
- the issue date is in the past
- the issue date is >= the vaccination date

# Problems left to tackle
- Key Distribution
- Revocation
